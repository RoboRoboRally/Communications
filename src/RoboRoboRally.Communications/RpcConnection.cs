﻿using Castle.DynamicProxy;
using Common.Logging;
using RoboRoboRally.Communications.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocketRPC;

namespace RoboRoboRally.Communications
{
    public class RpcConnection : IRpcConnection
    {
        ILog log = LogManager.GetLogger<RpcConnection>();
        readonly CancellationTokenSource cts = new CancellationTokenSource();

        public event Action<WebSocketCloseStatus, string> OnClose;

        Connection _connection;
        Connection Connection
        {
            get
            {
                return _connection;
            }
            set
            {
                _connection = value;
                proxyFactory = new CastleProxyFactory(Connection);
                value.OnReceive += message =>
                {
                    log.Debug($"Received message: {message}");
                    return Task.CompletedTask;
                };
                value.OnError += exception =>
                {
                    log.Debug($"Exception occured", exception);
                    if (exception is HttpListenerException)
                        value.CloseAsync(WebSocketCloseStatus.EndpointUnavailable);
                    return Task.CompletedTask;
                };
            }
        }

        IProxyFactory proxyFactory;

        static RpcConnection()
        {
            RPC.AddConverter(new InterfaceConverter());
            RPC.AddConverter(new TypeConverter());
            RPC.RpcTerminationDelay = new TimeSpan(-1);
        }

        public RpcConnection(Uri uri, Action<IRpcConnection> setup)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            Client.ConnectAsync(uri.OriginalString, cts.Token, connection =>
            {
                SetupConnection(connection, setup);
                tcs.SetResult(true); 
            });

            tcs.Task.Wait();
            log.Info("A new RpcConnection initialized");
        }       

        public RpcConnection(Connection connection, Action<IRpcConnection> connectionSetup)
        {
            SetupConnection(connection, connectionSetup);
        }

        private void SetupConnection(Connection connection, Action<IRpcConnection> connectionSetup)
        {
            Connection = connection;
            connection.OnClose += (status, description) => Task.Run(() => OnClose?.Invoke(status, description));
            connectionSetup(this);
        }

        private void CheckDisposed()
        {
            if (disposed)
                throw new InvalidOperationException("This connection has already been disposed");
        }

        public T GetRemote<T>() where T: class
        {
            CheckDisposed();
            return proxyFactory.CreateRemoteProxy<T>();
        }

        public void Register<T>(T implementation) where T : class
        {
            CheckDisposed();
            var proxy = proxyFactory.CreateLocalProxy(implementation);
            
            Connection.Bind<T>(proxy);            
        }

        #region IDisposable Support
        private bool disposed = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Connection.CloseAsync(WebSocketCloseStatus.NormalClosure).Wait();
                }                

                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
 