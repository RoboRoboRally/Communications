﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using WebSocketRPC;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace RoboRoboRally.Communications
{
    internal class RpcInterceptor<T> : IInterceptor
    {
        IRemoteBinder<T> remote;
        IProxyFactory proxyFactory;

        MethodInfo createExpressionMethod = typeof(RpcInterceptor<T>)
                .GetMethod(nameof(CreateExpression), BindingFlags.NonPublic | BindingFlags.Instance);
        MethodInfo callTaskMethod;
        MethodInfo callNonTaskMethod;

        public RpcInterceptor(IRemoteBinder<T> remote, IProxyFactory proxyFactory)
        {
            this.remote = remote;
            this.proxyFactory = proxyFactory;

            foreach(var method in typeof(IRemoteBinder<T>).GetMethods())
            {
                if (method.Name != nameof(remote.CallAsync) || !method.IsGenericMethod)
                    continue;

                var retType = method
                    .GetParameters()[0] // remote.CallAsync<>() first param - Expression
                    .ParameterType
                    .GetGenericArguments()[0] // Func<T, Tout>, TOut is either Task<U> or <U>
                    .GetGenericArguments()[1]; // TOut

                if (retType.IsGenericType && retType.GetGenericTypeDefinition() == typeof(Task<>))
                    callTaskMethod = method;
                else
                    callNonTaskMethod = method;
            }
        }
        
        public void Intercept(IInvocation invocation)
        {
            var returnType = invocation.Method.ReturnType;
            if (returnType == typeof(void))
            {
                CallVoidMethod(invocation).Wait();
            }
            else if (returnType == typeof(Task))
            {
                invocation.ReturnValue = CallVoidMethod(invocation);
            }
            else
            {
                CallFuncMethod(invocation);
            }            
        }

        private void CallFuncMethod(IInvocation invocation)
        {            
            Type returnType = invocation.Method.ReturnType;

            bool returningTask = returnType.IsGenericType && returnType.GetGenericTypeDefinition() == typeof(Task<>);
            Type genericType = returningTask ? returnType.GenericTypeArguments[0] : returnType;            

            Type funcType = typeof(Func<,>).MakeGenericType(typeof(T), returnType);

            var expression = createExpressionMethod
                .MakeGenericMethod(funcType)
                .Invoke(this, new[] { invocation });

            var method = returningTask ? callTaskMethod : callNonTaskMethod;
            method = method.MakeGenericMethod(genericType);

            var result = method.Invoke(remote, new[] { expression });
            
            if (genericType.IsInterface)
            {
                var proxy = CreateNewProxy(genericType);
                
                if (returningTask)
                {
                    var returnAsyncMethod = this
                        .GetType()
                        .GetMethod(nameof(ReturnAsync), BindingFlags.Instance | BindingFlags.NonPublic)
                        .MakeGenericMethod(genericType);

                    invocation.ReturnValue = returnAsyncMethod.Invoke(this, new object[] { result, proxy });
                }
                else
                {
                    typeof(Task<>)
                        .MakeGenericType(returnType)
                        .GetProperty(nameof(Task<T>.Result))
                        .GetValue(result); // wait for the task

                    invocation.ReturnValue = proxy;
                }
            }
            else
            {
                if (returningTask)
                {
                    invocation.ReturnValue = result; // return the task
                }
                else
                {
                    invocation.ReturnValue = typeof(Task<>)
                        .MakeGenericType(returnType)
                        .GetProperty(nameof(Task<T>.Result))
                        .GetValue(result); // wait for the task
                }
            }                 
        }

        private object CreateNewProxy(Type interfaceType)
        {
            var factoryMethod = typeof(IProxyFactory)
                .GetMethod(nameof(IProxyFactory.CreateRemoteProxy))
                .MakeGenericMethod(interfaceType);

            return factoryMethod.Invoke(proxyFactory, new object[] { });            
        }

        private Task<TRet> ReturnAsync<TRet>(Task tsk, TRet returnValue)
        {
            return tsk.ContinueWith(_ => returnValue);
        }

        private Task CallVoidMethod(IInvocation invocation)
        {
            return remote.CallAsync(CreateExpression<Action<T>>(invocation));
        }

        private Expression<TFunc> CreateExpression<TFunc>(IInvocation invocation)
        {
            var lambdaParam = Expression.Parameter(typeof(T));

            var method = invocation.Method;
            var args = new ConstantExpression[method.GetParameters().Length];
            for (int i = 0; i < args.Length; ++i)
            {
                args[i] = Expression.Constant(invocation.Arguments[i], method.GetParameters()[i].ParameterType);
            }            
            var call = Expression.Call(lambdaParam, invocation.GetConcreteMethod(), args);

            return Expression.Lambda<TFunc>(call, lambdaParam);
        }
    }
}
