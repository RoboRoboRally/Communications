﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocketRPC;

namespace RoboRoboRally.Communications
{
    public class RpcServer : IRpcServer
    {
        private int port;
        private bool useHttps;

        public RpcServer(int port, bool useHttps)
        {
            this.port = port;
            this.useHttps = useHttps;
        }

        public async Task AcceptAsync(CancellationToken cancelToken, Action<IRpcConnection> connectionSetup)
        {
            await Server.ListenAsync(port, cancelToken, (connection, context) =>
            {
                new RpcConnection(connection, connectionSetup);                        
            }, useHttps);
        }
    }
}
