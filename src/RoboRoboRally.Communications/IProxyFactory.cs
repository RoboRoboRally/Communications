﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Communications
{
    internal interface IProxyFactory
    {
        T CreateRemoteProxy<T>() where T : class;
        T CreateLocalProxy<T>(T implementation) where T : class;
    }
}
