﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

namespace RoboRoboRally.Communications
{
    public interface IRpcConnection : IDisposable
    {
        event Action<WebSocketCloseStatus, string> OnClose;
        void Register<T>(T implementation) where T : class;
        T GetRemote<T>() where T : class;
    }
}
