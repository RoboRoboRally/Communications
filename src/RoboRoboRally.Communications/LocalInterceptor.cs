﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Communications
{
    class LocalInterceptor : IInterceptor
    {
        IProxyFactory factory;

        public LocalInterceptor(IProxyFactory factory)
        {
            this.factory = factory;
        }

        public void Intercept(IInvocation invocation)
        {
            var methodParams = invocation.Method.GetParameters();

            for(int i = 0; i < methodParams.Length; ++i)
            {
                var paramType = methodParams[i].ParameterType;
                if (paramType.IsInterface && invocation.Arguments[i] == null)
                {
                    var factoryMethod = typeof(IProxyFactory).GetMethod(nameof(IProxyFactory.CreateRemoteProxy));
                    invocation.Arguments[i] = factoryMethod.MakeGenericMethod(paramType).Invoke(factory, Array.Empty<object>());
                }                    
            }

            invocation.Proceed();
        }
    }
}
