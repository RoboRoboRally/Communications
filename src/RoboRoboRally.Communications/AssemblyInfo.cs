﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: InternalsVisibleTo("RoboRoboRally.Communications.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]