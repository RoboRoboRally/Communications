﻿using Common.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoboRoboRally.Communications.Converters
{
    class TypeConverter : JsonConverter
    {
        ILog log = LogManager.GetLogger<TypeConverter>();

        public override bool CanConvert(Type objectType)
        {
            log.Debug($"Checking if {nameof(TypeConverter)} can convert {objectType.Name}");
            while (objectType != null)
            {
                if (objectType == typeof(Type))
                    return true;

                objectType = objectType.BaseType;
            }
            return false;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var typeName = reader.Value as string;
            log.Debug($"Trying to load type {typeName}");
            var type = AppDomain
                .CurrentDomain
                .GetAssemblies()
                .Select(a => a.GetType(typeName))
                .Where(t => t != null)
                .FirstOrDefault();

            if (type == null)
                throw new FormatException("Type is not present in current appdomain");
            return type;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {            
            var type = value as Type;
            if (type == null)
                writer.WriteNull();
            else
                writer.WriteValue(type.FullName);
        }
    }
}
