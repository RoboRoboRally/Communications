﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoboRoboRally.Communications.Converters
{
    /// <summary>
    /// Serializes and deserializes interface return types into nulls. (if an interface is returned, a new proxy object (RpcInterceptor) will be called.
    /// </summary>
    class InterfaceConverter : JsonConverter
    {
        /*CastleProxyFactory factory;

        public InterfaceConverter(CastleProxyFactory factory)
        {
            this.factory = factory;
        }*/

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsInterface;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteNull();
        }
    }
}
