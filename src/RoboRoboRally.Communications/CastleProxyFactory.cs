﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Text;
using WebSocketRPC;

namespace RoboRoboRally.Communications
{
    class CastleProxyFactory : IProxyFactory
    {
        ProxyGenerator generator = new ProxyGenerator();
        Connection connection;
        Dictionary<Type, object> alreadyCreatedRemotes = new Dictionary<Type, object>();

        public CastleProxyFactory(Connection connection)
        {
            this.connection = connection;
        }

        public T CreateRemoteProxy<T>() where T : class
        {
            if (alreadyCreatedRemotes.ContainsKey(typeof(T)))
                return (T)alreadyCreatedRemotes[typeof(T)];

            var binder = connection.Bind<T>();
            T remote = generator.CreateInterfaceProxyWithoutTarget<T>(new RpcInterceptor<T>(binder, this));

            alreadyCreatedRemotes.Add(typeof(T), remote);
            return remote;
        }        

        public T CreateLocalProxy<T>(T implementation) where T: class
        {
            return generator.CreateInterfaceProxyWithTarget<T>(implementation, new LocalInterceptor(this));
        }
    }
}
