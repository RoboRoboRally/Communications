﻿using Castle.DynamicProxy;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using WebSocketRPC;

namespace RoboRoboRally.Communications.Tests
{
    [TestFixture]
    public class RpcInterceptorTests
    {
        public interface TestInterface
        {
            void VoidMethod();
            void VoidMethodWithArg(int arg);
            void VoidMethodWithNullableArg(object arg);
            Task VoidMethodAsync();
            Task VoidMethodWithArgAsync(int arg);

            int FuncMethod();
            int FuncMethodWithArg(int arg);
            Task<int> FuncMethodAsync();
            Task<int> FuncMethodWithArgAsync(int arg);

            OtherInterface InterfaceMethod();
            Task<OtherInterface> InterfaceMethodAsync();
        }

        public interface OtherInterface
        {
        }

        Mock<IRemoteBinder<TestInterface>> remoteBinder;
        Mock<IProxyFactory> proxyFactory;
        Mock<OtherInterface> otherInterface;
        TestInterface testInterface;

        [SetUp]
        public void Init()
        {
            remoteBinder = new Mock<IRemoteBinder<TestInterface>>();
            proxyFactory = new Mock<IProxyFactory>();
            otherInterface = new Mock<OtherInterface>();
            testInterface = new ProxyGenerator()
                .CreateInterfaceProxyWithoutTarget<TestInterface>(new RpcInterceptor<TestInterface>(remoteBinder.Object, proxyFactory.Object));            
        }

        private void AssertLambdasEqual<T>(Expression<T> expected, Expression<T> actual)
        {
            actual = actual.Rewrite(expected.Parameters[0].Name);

            Assert.AreEqual(expected.ToString(), actual.ToString());
        }

        #region void methods

        private void SetupExpectation(Expression<Action<TestInterface>> expectation, Task returnedTask = null)
        {
            returnedTask = returnedTask ?? Task.CompletedTask;
            remoteBinder.Setup(s => s.CallAsync(It.IsAny<Expression<Action<TestInterface>>>()))
                .Callback<Expression<Action<TestInterface>>>(expr =>
                {
                    AssertLambdasEqual(expectation, expr);
                })
                .Returns(returnedTask);
        }

        private void RunTest(Expression<Action<TestInterface>> expectation)
        {
            SetupExpectation(expectation);

            expectation.Compile().Invoke(testInterface);
        }

        [Test]
        public void CallVoidMethod()
        {
            RunTest(o => o.VoidMethod());  
        }

        [Test]
        public void CallVoidMethodWithArg()
        {
            RunTest(o => o.VoidMethodWithArg(5));
        }

        [Test]
        public void CallVoidMethodWithNullableArg()
        {
            RunTest(o => o.VoidMethodWithNullableArg(null));
        }

        [Test]
        public void CallVoidAsyncMethod()
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            SetupExpectation(o => o.VoidMethodAsync(), tcs.Task);

            var result = testInterface.VoidMethodAsync();
            Assert.IsFalse(result.IsCompleted);
        }

        [Test]
        public void CallVoidMethodAsyncMethodWithArg()
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            SetupExpectation(o => o.VoidMethodWithArgAsync(5), tcs.Task);

            var result = testInterface.VoidMethodWithArgAsync(5);
            Assert.IsFalse(result.IsCompleted);
        }

        #endregion

        #region methods with return value

        private void SetupFuncTest(Expression<Func<TestInterface, int>> expectedLambda, int expectedResult)
        {
            remoteBinder.Setup(s => s.CallAsync(It.IsAny<Expression<Func<TestInterface, int>>>()))
                .Callback<Expression<Func<TestInterface, int>>>(expr =>
                {
                    AssertLambdasEqual(expectedLambda, expr);
                })
                .Returns(Task.FromResult(expectedResult));            
        }

        private void SetupFuncTest(Expression<Func<TestInterface, Task<int>>> expectedLambda, Task<int> expectedResult)
        {
            remoteBinder.Setup(s => s.CallAsync(It.IsAny<Expression<Func<TestInterface, Task<int>>>>()))
                .Callback<Expression<Func<TestInterface, Task<int>>>>(expr =>
                {
                    AssertLambdasEqual(expectedLambda, expr);
                })
                .Returns(expectedResult);
        }

        [Test]
        public void CallFunc()
        {
            int expectedResult = 5;
            SetupFuncTest(o => o.FuncMethod(), expectedResult);

            var result = testInterface.FuncMethod();
            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void CallFuncWithArg()
        {
            int expectedResult = 5;
            const int arg = 3;
            SetupFuncTest(o => o.FuncMethodWithArg(arg), expectedResult);            

            var result = testInterface.FuncMethodWithArg(arg);
            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void CallFuncAsync()
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            SetupFuncTest(o => o.FuncMethodAsync(), tcs.Task);

            var result = testInterface.FuncMethodAsync();
            Assert.IsFalse(result.IsCompleted);
            Assert.AreEqual(tcs.Task.Id, result.Id);
        }

        [Test]
        public void CallFuncAsyncWithArg()
        {
            Task<int> tsk = Task.Run(() => { Thread.Sleep(1000); return 5; });
            const int arg = 3;
            SetupFuncTest(o => o.FuncMethodWithArgAsync(arg), tsk);

            var result = testInterface.FuncMethodWithArgAsync(arg);
            Assert.IsFalse(result.IsCompleted);
            Assert.AreEqual(tsk.Id, result.Id);
        }        

        [Test]
        public void CallFuncReturningInterface()
        {
            proxyFactory.Setup(f => f.CreateRemoteProxy<OtherInterface>()).Returns(otherInterface.Object);

            Expression<Func<TestInterface, OtherInterface>> expectedLambda = o => o.InterfaceMethod();
            remoteBinder.Setup(s => s.CallAsync(It.IsAny<Expression<Func<TestInterface, OtherInterface>>>()))
                .Callback<Expression<Func<TestInterface, OtherInterface>>>(expr =>
                {
                    AssertLambdasEqual(expectedLambda, expr);
                })
                .Returns(Task.FromResult(otherInterface.Object));

            var result = testInterface.InterfaceMethod();

            Assert.AreEqual(result, otherInterface.Object);
        }

        [Test]
        public void CallAsyncFuncReturningInterface()
        {
            proxyFactory.Setup(f => f.CreateRemoteProxy<OtherInterface>()).Returns(otherInterface.Object);

            Expression<Func<TestInterface, Task<OtherInterface>>> expectedLambda = o => o.InterfaceMethodAsync();
            TaskCompletionSource<OtherInterface> tcs = new TaskCompletionSource<OtherInterface>();
            remoteBinder.Setup(s => s.CallAsync(It.IsAny<Expression<Func<TestInterface, Task<OtherInterface>>>>()))
                .Callback<Expression<Func<TestInterface, Task<OtherInterface>>>>(expr =>
                {
                    AssertLambdasEqual(expectedLambda, expr);
                })
                .Returns(tcs.Task);
            
            var result = testInterface.InterfaceMethodAsync();

            Assert.IsFalse(result.IsCompleted);
        }

        #endregion
    }
}
