﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace RoboRoboRally.Communications.Tests
{
    static class ExpressionExtensions
    {
        public static Expression<T> Rewrite<T>(this Expression<T> exp, string newParamName)
        {
            var param = Expression.Parameter(exp.Parameters[0].Type, newParamName);
            var newExpression = new PredicateRewriterVisitor(param).Visit(exp);

            return (Expression<T>)newExpression;
        }
    }

    internal class PredicateRewriterVisitor : ExpressionVisitor
    {
        private readonly ParameterExpression parameterExpression;

        public PredicateRewriterVisitor(ParameterExpression parameterExpression)
        {
            this.parameterExpression = parameterExpression;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return parameterExpression;
        }
    }
}
